package rest.tasks;

import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Get;
import net.thucydides.core.annotations.Step;

import static net.serenitybdd.screenplay.Tasks.instrumented;

import net.serenitybdd.screenplay.Actor;

public class FindPet implements Task {
	private final long id;

	public FindPet(long id) {
		this.id = id;
	}

	public static FindPet withId(long id) {
		return instrumented(FindPet.class, id);
	}

	@Override
	@Step("{0} fetches the pet with id #id")
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Get.resource("/pet/{id}").with(request -> request.pathParam("id", id)));
	}
}