package rest.definitions;

import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.emptyString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

import java.util.List;

import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.serenitybdd.screenplay.rest.interactions.Delete;
import net.serenitybdd.screenplay.rest.interactions.Get;
import net.serenitybdd.screenplay.rest.interactions.Post;
import net.serenitybdd.screenplay.rest.interactions.Put;
import net.thucydides.core.annotations.Shared;
import net.thucydides.core.util.EnvironmentVariables;
import rest.models.Pet;
import rest.tasks.FindPet;

public class RestDefinitions {
	private String theRestApiBaseUrl;
	private EnvironmentVariables environmentVariables;
	private Actor sam;
	@Shared
	private Pet newPet;

	@Before
	public void configureBaseUrl() {
		theRestApiBaseUrl = environmentVariables.optionalProperty("restapi.baseurl")
				.orElse("https://petstore.swagger.io/v2");

		sam = Actor.named("Sam the supervisor").whoCan(CallAnApi.at(theRestApiBaseUrl));

		newPet.setName("Parrot");
		newPet.setStatus("pending");
		// !!!BUG!!!
		// TODO: fix bug
		// !!!The only way to have a working get by id is by forcing the id in
		// post, it assigns 64bit id's but the get doesn't support them!!!
		// !!!Even if it's on the description!!!
		newPet.setId(96967795);
	}

	@Given("{actor} has access to available pets")
	public void hasAccessToPets(Actor actor) {
		sam.attemptsTo(Get.resource("/pet/findByStatus?status=available"));
	}

	@When("{actor} attempts to get all available pets")
	public void attempsToListAllAvailablePets(Actor actor) {
		sam.attemptsTo(Get.resource("/pet/findByStatus?status=available"));
	}

	@Then("{actor} gets all available pets")
	public void listAllAvailablePets(Actor actor) {
		sam.should(seeThatResponse("all the expected available pets should be returned",
				response -> response.statusCode(200).body("name", hasItems("doggie", "fish"))));

		List<String> pets = SerenityRest.lastResponse().jsonPath().getList("status", String.class);

		assertThat(pets).size().isGreaterThan(100);
		assertThat(pets).allMatch(item -> item.equals("available"));
	}

	@Given("{actor} has added a new pet")
	public void hasAddedAPet(Actor actor) {
		sam.attemptsTo(FindPet.withId(newPet.getId()));

		// !!!FIXME: fix test
		// casting to int just to show the test by working arround the bug
		sam.should(seeThatResponse("User details should be correct", response -> response.statusCode(200)
				.body("id", equalTo((int) newPet.getId())).body("name", equalTo(newPet.getName()))));
		SerenityRest.lastResponse().body();
	}

	@When("{actor} adds a new {string} pet")
	public void addsPet(Actor actor, String status) {
		sam.attemptsTo(
				Post.to("/pet").with(request -> request.header("Content-Type", "application/json").body(newPet)));
	}

	@Then("{actor} sees a new pet has been added")
	public void hasAddedPet(Actor actor) {
		sam.should(seeThatResponse(response -> response.statusCode(200).body("id", is(not(0l)))));

		newPet.setId(SerenityRest.lastResponse().jsonPath().getLong("id"));
	}

	@When("{actor} updates the pet status to {string}")
	public void updatesPetStatus(Actor actor, String status) {
		newPet.setStatus(status);

		sam.attemptsTo(Put.to("/pet").with(request -> request.header("Content-Type", "application/json").body(newPet)));

		sam.should(seeThatResponse(response -> response.statusCode(200).body("updatedAt", is(not(emptyString())))));
	}

	@Then("{actor} sees the pet status is {string}")
	public void hasPutPet(Actor actor, String status) {
		sam.attemptsTo(FindPet.withId(newPet.getId()));

		sam.should(seeThatResponse(response -> response.statusCode(200).body("status", is(status))));
	}

	@When("{actor} deletes the pet")
	public void deletePet(Actor actor) {
		sam.attemptsTo(Delete.from("/pet/{id}").with(request -> request.pathParam("id", newPet.getId())));
	}

	@Then("{actor} sees the pet is deleted")
	public void hasDeletedPet(Actor actor) {
		sam.should(seeThatResponse(response -> response.statusCode(200)));
	}

}
