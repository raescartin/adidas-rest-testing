Feature: Pets

  Scenario: Get "available" pets
    Given Bob has access to available pets 
    When he attempts to get all available pets
    Then he gets all available pets
  
  Scenario: Post a new "available" pet
    Given Bob has access to available pets 
    When he adds a new "available" pet
    Then he sees a new pet has been added
    
  Scenario: Update pet status to "sold"
    Given Bob has added a new pet
    When he updates the pet status to "sold"
    Then he sees the pet status is "sold"
    
  Scenario: Delete pet
  	Given Bob has added a new pet
  	When he deletes the pet
  	Then he sees the pet is deleted
    
