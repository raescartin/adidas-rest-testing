# Adidas REST testing challenge

This is a simple project using Serenity with Cucumber and RestAssured

## Executing the tests
To run the sample project, you can either just run the `CucumberTestSuite` test runner class, or run either `mvn verify` or `gradle test` from the command line.

## Bugs
A bug was found where 32 bit integers are used instead of 64 bit as documented
It's commented in the code
There's some tunning done to have stable tests I would NEVER do testing production code

## TODO:
- fix the bug
- fix the tests by removing forcing the id of a pet on POST and the cast to (int)
- use more Task classes for abstraction and layering as seen in FindPet
- improve actor definition
